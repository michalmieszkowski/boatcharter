package com.sda.boatcharter.boat.repository;

import com.sda.boatcharter.boat.Boat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BoatRepository extends JpaRepository<Boat, Long> {

    Boat save(Boat boat);

    Optional<Boat> findById(Long id);

    List<Boat> findAll();

    void deleteById(Long id);

    boolean existsById(Long boatId);

    @Query("SELECT b from Boat b where reservation_id = :reservationId")
    Optional<Boat> findBoatByReservationId(@Param("reservationId") Long reservationId);


    @Query("SELECT b from Boat b where localization_id = :localizationId")
    List<Boat> findBoatByLocalizationId(@Param("localizationId") Long localizationId);

}
