package com.sda.boatcharter.boat;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/boat")
@CrossOrigin (value = "http://localhost:8000")
public class BoatController {

    private final BoatService boatService;

    @Autowired
    public BoatController(BoatService boatService) {
        this.boatService = boatService;
    }


    @GetMapping
    public ResponseEntity<List<Boat>> showAllBoats(@RequestParam (value = "available", required = false) Boolean available) {
        if(null == available) {
            return ResponseEntity.ok(boatService.showAllBoats());
        } else
            return ResponseEntity.ok(boatService.showAvailableBoats());
    }

    @GetMapping("/{boatId}")
    public ResponseEntity<Boat> showBoatById(@PathVariable("boatId") Long boatId){
        return ResponseEntity.ok(boatService.showBoatById(boatId).orElse(null));
    }

    @PostMapping
    public ResponseEntity<Boat> addNewBoat(@RequestBody Boat boat){
        return new ResponseEntity<>(boatService.saveBoat(boat), HttpStatus.CREATED);
    }

    @DeleteMapping("/{boatId}")
    public ResponseEntity<?> deleteBoatById(@PathVariable ("boatId") Long boatId){
        if (boatService.deleteBoatById(boatId)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{boatId}")
    public ResponseEntity<Boat> editBoat(@PathVariable ("boatId") Long boatId, @RequestBody Boat boat){
        return new ResponseEntity<>(boatService.updateBoat(boat), HttpStatus.OK);
    }

}
