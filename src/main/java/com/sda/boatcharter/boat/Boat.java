package com.sda.boatcharter.boat;

import com.sda.boatcharter.localization.Localization;
import com.sda.boatcharter.reservation.Reservation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Boat {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Float length;

    private Float width;

    private Float submergence;

    private Float waterTank;

    private Float fuelTank;

    private Integer cabin;

    private Integer berth;

    private Integer toilet;

    private Boolean availability;

    private Float price;

    @OneToOne (orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn (name = "reservation_id")
    private Reservation reservation;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "localization_id")
    private Localization localization;

}
