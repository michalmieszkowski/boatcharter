package com.sda.boatcharter.boat;

import com.sda.boatcharter.boat.repository.BoatRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BoatService {

    private final BoatRepository boatRepository;

    public BoatService(BoatRepository boatRepository) {
        this.boatRepository = boatRepository;
    }

    public Optional<Boat> showBoatById(Long boatId){
      return boatRepository.findById(boatId);
    }

    public List<Boat> showAllBoats(){
        return boatRepository.findAll();
    }

    public List<Boat> showAvailableBoats(){
        return boatRepository.findAll().stream()
                .filter(Boat::getAvailability)
                .collect(Collectors.toList());
    }

    public boolean deleteBoatById(Long boatId) {
        if(boatRepository.existsById(boatId)) {
            boatRepository.deleteById(boatId);
            return true;
        } else {
            return false;
        }
    }

    public Boat saveBoat(Boat boat){
        return boatRepository.save(boat);
    }

    public Boat updateBoat(Boat boat){
        return boatRepository.save(boat);
    }






}
