package com.sda.boatcharter.reservation;

import com.sda.boatcharter.boat.BoatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reservation")
@CrossOrigin (value = "http://localhost:8000")
public class ReservationController {

    private final ReservationService reservationService;
    private final BoatService boatService;

    @Autowired
    public ReservationController(ReservationService reservationService, BoatService boatService) {
        this.reservationService = reservationService;
        this.boatService = boatService;
    }

    @GetMapping
    public ResponseEntity<List<Reservation>> showAllReservations(){
        return new ResponseEntity<>(reservationService.showAllReservations(), HttpStatus.OK);
    }

    @GetMapping ("/{reservationId}")
    public ResponseEntity<Reservation> showReservation(@PathVariable ("reservationId") Long reservationId) {
        if(null == reservationService.showReservation(reservationId)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(reservationService.showReservation(reservationId), HttpStatus.OK);
        }
    }

    @DeleteMapping("/{reservationId}")
    public ResponseEntity<String> deleteReservation(@PathVariable ("reservationId") Long reservationId) {
        reservationService.deleteReservation(reservationId);
        return new ResponseEntity<>("Reservation with id = " + reservationId + " removed", HttpStatus.OK);
    }

    @PostMapping ("boat/{boatId}")
    public ResponseEntity<Reservation> addNewReservation(@PathVariable ("boatId") Long boatId, @RequestBody Reservation reservation) {
        if (null == reservationService.addNewReservation(boatId, reservation)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(reservationService.addNewReservation(boatId, reservation), HttpStatus.OK);
        }
    }
}
