package com.sda.boatcharter.reservation;

import com.sda.boatcharter.boat.Boat;
import com.sda.boatcharter.boat.repository.BoatRepository;
import com.sda.boatcharter.reservation.Reservation;
import com.sda.boatcharter.reservation.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private final BoatRepository boatRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, BoatRepository boatRepository) {
        this.reservationRepository = reservationRepository;
        this.boatRepository = boatRepository;
    }

    public List<Reservation> showAllReservations(){
        return reservationRepository.findAll();
    }

    public Reservation addNewReservation(Long boatId, Reservation reservation){
        Optional<Boat> boat = boatRepository.findById(boatId);
        if(boat.isPresent()){
            boat.get().setReservation(reservation);
            boat.get().setAvailability(false);
            return reservationRepository.save(reservation);
        } else
            return null;
    }

    public void deleteReservation(Long reservationId){
        Boat boat = boatRepository.findBoatByReservationId(reservationId).orElse(null);
        reservationRepository.deleteById(reservationId);
        boat.setReservation(null);
        boat.setAvailability(true);
        boatRepository.save(boat);

    }

    public Reservation showReservation(Long reservationId){
        Optional<Reservation> reservation = reservationRepository.findById(reservationId);
        return reservation.orElse(null);
    }


}
