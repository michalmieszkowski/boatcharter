package com.sda.boatcharter.localization.repository;

import com.sda.boatcharter.localization.Localization;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface LocalizationRepository extends CrudRepository<Localization, Long> {

    Localization save(Localization localization);

    Optional<Localization> findById(Long id);

    List<Localization> findAll();

    void deleteById(Long id);


}
