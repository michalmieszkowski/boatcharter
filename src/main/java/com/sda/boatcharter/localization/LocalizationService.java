package com.sda.boatcharter.localization;

import com.sda.boatcharter.boat.Boat;
import com.sda.boatcharter.boat.repository.BoatRepository;
import com.sda.boatcharter.localization.Localization;
import com.sda.boatcharter.localization.repository.LocalizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class LocalizationService {

    private final LocalizationRepository localizationRepository;
    private final BoatRepository boatRepository;

    @Autowired
    public LocalizationService(LocalizationRepository localizationRepository, BoatRepository boatRepository) {
        this.localizationRepository = localizationRepository;
        this.boatRepository = boatRepository;
    }


    public Localization addNewLocalization(Localization localization){
        return localizationRepository.save(localization);
    }

    public void deleteLocalization(Long localizationId){

        List<Boat> boats = boatRepository.findBoatByLocalizationId(localizationId);
        for (Boat b: boats) {
            b.setLocalization(null);
            boatRepository.save(b);
        }
        localizationRepository.deleteById(localizationId);
    }

    public List<Localization> showAllLocalizations(){
        return localizationRepository.findAll();
    }

    public Localization showLocalizationById(Long localizationId){
        return localizationRepository.findById(localizationId).orElse(null);
                //.orElseThrow(()-> new EntityNotFoundException("Entity with id = " + localizationId + " doesnt exist"));
    }

    public Localization addBoatToLocalization(Long localizationId, Long boatId) {
        Optional<Boat> boat = boatRepository.findById(boatId);
        Optional<Localization> localization = localizationRepository.findById(localizationId);
        if (boat.isPresent() && localization.isPresent()) {
            boat.get().setLocalization(localization.get());
            boatRepository.save(boat.get());
            return localization.get();
        } else
            return null;
    }


}
