package com.sda.boatcharter.localization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/localization")
@CrossOrigin (value = "http://localhost:8000")
public class LocalizationController {

    private final LocalizationService localizationService;

    @Autowired
    public LocalizationController(LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    @GetMapping
    public ResponseEntity<List<Localization>> showAllLocalizations(){
        return new ResponseEntity<>(localizationService.showAllLocalizations(), HttpStatus.OK);
    }

    @GetMapping("/{localizationId}")
    public ResponseEntity<Localization> showLocalizationById(@PathVariable ("localizationId") Long localizationId){
        if(null == localizationService.showLocalizationById(localizationId)){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(localizationService.showLocalizationById(localizationId), HttpStatus.OK);
        }
    }

    @PostMapping
    public ResponseEntity<Localization> addNewLocalization(@RequestBody Localization localization){
        return new ResponseEntity<>(localizationService.addNewLocalization(localization), HttpStatus.CREATED);
    }

    @DeleteMapping("/{localizationId}")
    public ResponseEntity<String> deleteLocalization(@PathVariable ("localizationId") Long localizationId){
        localizationService.deleteLocalization(localizationId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{localizationId}/boat/{boatId}")
    public ResponseEntity<Localization> addBoatToLocalization(@PathVariable ("localizationId") Long localizationId, @PathVariable Long boatId){
        return new ResponseEntity<>(localizationService.addBoatToLocalization(localizationId, boatId), HttpStatus.OK);
    }



}
