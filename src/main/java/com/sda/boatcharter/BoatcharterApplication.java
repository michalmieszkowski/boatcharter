package com.sda.boatcharter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoatcharterApplication  {



    public static void main(String[] args) {
        SpringApplication.run(BoatcharterApplication.class, args);
    }


}
