package com.sda.boatcharter.boatFiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Service
public class BoatFileService {

    private final BoatFileProperties boatFileProperties;

    @Autowired
    public BoatFileService(BoatFileProperties boatFileProperties) {
        this.boatFileProperties = boatFileProperties;
    }

    public void storeFile(MultipartFile file, Long boatId) throws IOException {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String boatDirectory = boatFileProperties.getUploadDir() + "/" + boatId;
        Path path = Paths.get(boatDirectory);
        Files.createDirectories(path);
        Files.copy(file.getInputStream(), path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
    }

    public Resource loadBoatImage(String filename, Long boatId) throws MalformedURLException, FileNotFoundException {

        String boatDirectory = boatFileProperties.getUploadDir() + "/" + boatId;
        Path path = Paths.get(boatDirectory).resolve(filename).normalize();
        Resource resource = new UrlResource(path.toUri());
        if(resource.exists()){
            return resource;
        } else {
            throw new FileNotFoundException();
        }


    }
}
