package com.sda.boatcharter.boatFiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@RestController
@RequestMapping("/boat/images")
public class BoatFileController {

    private final BoatFileService boatFileService;

    @Autowired
    public BoatFileController(BoatFileService boatFileService) {
        this.boatFileService = boatFileService;
    }

    @GetMapping("/{boatId}/{fileName}")
    public Resource getBoatImage(@PathVariable ("boatId") Long boatId, @PathVariable ("fileName") String fileName) throws MalformedURLException, FileNotFoundException {
        return boatFileService.loadBoatImage(fileName, boatId);

    }

    @PostMapping ("/{boatId}")
    public ResponseEntity<?> uploadBoatImage(@RequestParam ("image")  MultipartFile image, @PathVariable ("boatId") Long boatId) throws IOException {
        boatFileService.storeFile(image, boatId);
        return ResponseEntity.ok().build();
    }




}
